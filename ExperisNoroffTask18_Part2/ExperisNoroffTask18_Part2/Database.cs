﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SQLite;
using System.IO;

namespace ExperisNoroffTask18_Part2
{
    class Database
    {
        public SQLiteConnection myConnection;
        //Constructor
        public Database()
        {
            myConnection = new SQLiteConnection("Data Source=Task15Database.db; Version = 3; New = True; Compress = True; ");
            if (!File.Exists("./database.sqlite3"))
            {
                SQLiteConnection.CreateFile("database.sqlite3");
            }
        }
        // Open DB
        public void OpenDB()
        {
            if (myConnection.State != System.Data.ConnectionState.Open)
            {
                myConnection.Open();
            }
        }
        //Close B
        public void CloseDB()
        {
            if (myConnection.State != System.Data.ConnectionState.Closed)
            {
                myConnection.Close();
            }
        }
    }
}
