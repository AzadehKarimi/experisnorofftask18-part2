﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SQLite;
using Task17;

namespace ExperisNoroffTask18_Part2
{
    public partial class Form1 : Form
    {
        Database databaseobject = new Database();
        string id = "";
        public Form1()
        {
            InitializeComponent();
            ShowAllData();
        }
        private void Form1_Load(object sender, EventArgs e)
        {
            GenderCB.Items.Add("Male");
            GenderCB.Items.Add("Female");
            CharacterCB.Items.Add("Worrior");
            CharacterCB.Items.Add("Wizard");
            CharacterCB.Items.Add("Thief");
        }
        private void Label1_Click(object sender, EventArgs e)
        {

        }
        //Update
        private void UpdateBtn_Click(object sender, EventArgs e)
        {
            try
            {
                if (id != "")
                {
                    DialogResult d = MessageBox.Show("Do You Want To Update Data for ID:" + id + " ?", "Update Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (d == DialogResult.Yes)
                    {
                        try
                        {
                            //Query
                            string query = "Update Character SET Type='"+CharacterCB.SelectedItem.ToString()+"',Name='" +NameTxt.Text+"',Hp='"+HpTxt.Text+"',Energy='"+EnergyTxt.Text+"',armorRating='"+armorTxt.Text+"',gender='"+GenderCB.SelectedItem.ToString() +"'WHERE id= '"+id+"'";
                            //Command
                            SQLiteCommand cmd = new SQLiteCommand(query, databaseobject.myConnection);
                            //Connection open
                            databaseobject.OpenDB();
                            // Command Execuation
                            int check = cmd.ExecuteNonQuery();
                            if (check > 0)
                            {
                                MessageBox.Show("Update Successfull");
                                Clear();
                                ShowAllData();
                            }
                            //Connection Close
                            databaseobject.CloseDB();
                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("First You Have to Select From DataGrid");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        // Add new info
        private void InsertClick(object sender, EventArgs e)
        {
            string name = NameTxt.Text;
            string gender = GenderCB.SelectedItem.ToString();
            string characterType = CharacterCB.SelectedItem.ToString();

            if (name != "")
            {
                try
                {
                    //Query
                    string query = $"INSERT INTO Character( Type, Name,Hp,Energy,armorRating,gender) VALUES ( '" + CharacterCB.SelectedItem.ToString() + "','" + NameTxt.Text + "','" + HpTxt.Text + "','" + EnergyTxt.Text + "','" + armorTxt.Text + "','" + GenderCB.SelectedItem.ToString() + "')";
                    //Command
                    SQLiteCommand cmd = new SQLiteCommand(query, databaseobject.myConnection);
                    //Connection open
                    databaseobject.OpenDB();
                    // Command Execuation
                    int check = cmd.ExecuteNonQuery();
                    if (check > 0)
                    {
                        MessageBox.Show("Insert Successfull");
                        Clear();
                        ShowAllData();
                    }
                    //Connection Close
                    databaseobject.CloseDB();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
            else
            {
                MessageBox.Show("Name Should NOt be Empty");
            }
            // MessageBox.Show("Add Button Clicked");

        }
        // Field Clear
        public void Clear()
        {
            NameTxt.Text = "";
            CharacterCB.SelectedItem = "";
            GenderCB.SelectedItem = "";
            HpTxt.Text = "";
            EnergyTxt.Text = "";
            armorTxt.Text = "";
            id = "";
        }
        // show All Data
        public void ShowAllData()
        {
            try
            {
                // Query
                string query = "SELECT * FROM  Character";
                //Command
                SQLiteCommand cmd = new SQLiteCommand(query, databaseobject.myConnection);
                //Connection open
                databaseobject.OpenDB();
                // Command Execuation

                SQLiteDataReader result = cmd.ExecuteReader();
                if (result.HasRows)
                {
                    Grid.Rows.Clear();
                    while (result.Read())
                    {
                        int n = Grid.Rows.Add();
                        Grid.Rows[n].Cells[0].Value = result[0].ToString();
                        Grid.Rows[n].Cells[1].Value = result[1].ToString();
                        Grid.Rows[n].Cells[2].Value = result[2].ToString();
                        Grid.Rows[n].Cells[3].Value = result[3].ToString();
                        Grid.Rows[n].Cells[4].Value = result[4].ToString();
                        Grid.Rows[n].Cells[5].Value = result[5].ToString();
                        Grid.Rows[n].Cells[6].Value = result[6].ToString();
                    }
                }
                //Connection Close
                databaseobject.CloseDB();
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }


        private void TextBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void Label7_Click(object sender, EventArgs e)
        {

        }

        // Search
        private void TextBox1_TextChanged_1(object sender, EventArgs e)
        {
            try
            {
                // Query
                string query = "SELECT * FROM  Character WHERE name like '%" + textBox1.Text + "%'";
                //Command
                SQLiteCommand cmd = new SQLiteCommand(query, databaseobject.myConnection);
                //Connection open
                databaseobject.OpenDB();
                // Command Execuation
                Grid.Rows.Clear();
                SQLiteDataReader result = cmd.ExecuteReader();
                if (result.HasRows)
                {

                    while (result.Read())
                    {
                        int n = Grid.Rows.Add();
                        Grid.Rows[n].Cells[0].Value = result[0].ToString();
                        Grid.Rows[n].Cells[1].Value = result[1].ToString();
                        Grid.Rows[n].Cells[2].Value = result[2].ToString();
                        Grid.Rows[n].Cells[3].Value = result[3].ToString();
                        Grid.Rows[n].Cells[4].Value = result[4].ToString();
                        Grid.Rows[n].Cells[5].Value = result[5].ToString();
                        Grid.Rows[n].Cells[6].Value = result[6].ToString();
                    }
                }
                //Connection Close
                databaseobject.CloseDB();
            }

            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        // Show All Button
        private void ShowAllBtn_Click(object sender, EventArgs e)
        {
            ShowAllData();
        }
        // Data Grid Click
        private void GridClick(object sender, EventArgs e)
        {
            id = Grid.SelectedRows[0].Cells[0].Value.ToString();
            CharacterCB.Text = Grid.SelectedRows[0].Cells[1].Value.ToString();
            NameTxt.Text = Grid.SelectedRows[0].Cells[2].Value.ToString();
            HpTxt.Text = Grid.SelectedRows[0].Cells[3].Value.ToString();
            EnergyTxt.Text = Grid.SelectedRows[0].Cells[4].Value.ToString();
            armorTxt.Text = Grid.SelectedRows[0].Cells[5].Value.ToString();
            GenderCB.Text = Grid.SelectedRows[0].Cells[6].Value.ToString();
            //MessageBox.Show(id);
        }

        private void DeleteClick(object sender, EventArgs e)
        {
            try
            {
                if (id != "")
                {
                    DialogResult d = MessageBox.Show("Do You Want To Delete All Data forID:"+id+" ?", "Delete Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                    if (d == DialogResult.Yes)
                    {
                        try
                        {
                            //Query
                            string query ="DELETE FROM Character WHERE id='"+id+"'";
                            //Command
                            SQLiteCommand cmd = new SQLiteCommand(query, databaseobject.myConnection);
                            //Connection open
                            databaseobject.OpenDB();
                            // Command Execuation
                            int check = cmd.ExecuteNonQuery();
                            if (check > 0)
                            {
                                MessageBox.Show("Delete Successfull");
                                Clear();
                                ShowAllData();
                            }
                            //Connection Close
                            databaseobject.CloseDB();
                        }
                        catch (Exception ex)
                        {

                            MessageBox.Show(ex.Message);
                        }
                    }
                }
                else
                {
                    MessageBox.Show("First You Have to Select From DataGrid");
                }
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void Label5_Click(object sender, EventArgs e)
        {

        }
    }
}
